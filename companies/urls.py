# -*- coding: utf-8 -*-

from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^stats/$', views.company_stats_api_view, name='company_stats_api_view'),
    url(r'^stats/view/$', views.company_stats_view, name='company_stats_view'),
    url(r'^stats/api/view/$', views.company_stats_api_view, name='company_stats_api_view'),
    url(r'^monitor/([0-9]+)/$', views.company_to_monitor, name='company_to_monitor'),
    url(r'^monitoring/$', views.currently_monitored_companies, name='currently_monitored_companies'),
]
