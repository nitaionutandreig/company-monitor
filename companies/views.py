# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.http import JsonResponse
from django.shortcuts import render
from django.contrib.auth.models import User

from .models import Company

def most_recently_founded_companies(limit=10):
    companies = []
    for comp in Company.objects.all():
        serialized = {
            'companies_house_id': comp.companies_house_id,
            'name': comp.name,
            'description': comp.description,
            'date_founded': comp.date_founded,
            'country__iso_code': comp.country.iso_code,
            'creator__username': comp.creator.username,
        }
        companies.append(serialized)

    if limit:
        companies = companies[:limit]
    return sorted(companies, key=lambda comp: comp['date_founded'])

def top_level_stats():
    import statistics
    '''Create an unauthenticated API end point which returns some top level stats (pick a few of these):
        * The 10 most recently founded companies
        * Average employee count
        * Breakdown of number of companies founded per quarter for the last five years
        * User who has created the most companies
        * User with the greatest total number of employees at all companies they have created
        * Average deal amount raised by country (i.e. deals for companies in those countries)
        * Any other ideas you think may be interesting'''
    all_comps = Company.objects.all()
    all_users = User.objects.all()
    avg_emp_count = statistics.mean([len(comp.employee_set.values()) for comp in all_comps])
    user_most_comp = max([{"user": u.username, "no": len(u.companies_created.values())} for u in all_users], key=lambda x:x['no'])
    return avg_emp_count, user_most_comp


def company_stats_api_view(request): 
    avg_emp_count, user_most_comp = top_level_stats()
    response = {
        'most_recently_founded': most_recently_founded_companies(),
        # NOTE: The below is dummy data so you can work on the front end without
        # building out the API first. Replace the dummy data below once you've
        # built functional replacements.
        'average_employee_count': avg_emp_count,
        'companies_founded_per_quarter': [
            {'year': 2017, 'quarter': 1, 'value': 5},
            {'year': 2017, 'quarter': 2, 'value': 10},
            {'year': 2017, 'quarter': 3, 'value': 3},
            {'year': 2017, 'quarter': 4, 'value': 15},
            {'year': 2018, 'quarter': 1, 'value': 24},
        ],
        'user_created_most_companies': user_most_comp,
        'user_created_most_employees': "Jane",
        'average_deal_amount_raised_by_country': [
            {'country': 'gb', 'average_deal_amount_raised': 500.0},
            {'country': 'fr', 'average_deal_amount_raised': 450.0},
        ]
    }
    return JsonResponse(response)

def company_to_monitor(request, comp_id):
    # Create an API end point which allows authenticated users 
    # to pass in the id of a company to monitor.
    if request.user.is_authenticated():
        comp = Company.objects.get(id=comp_id)
        user = request.user.id
        comp.monitors.add(user)
        response = {"message": "You are now monitoring company {}".format(comp.name)}
    else:
        response = {"error": "Unauthorized request."}
    return JsonResponse(response)

def currently_monitored_companies(request):
    # Create an API end point which allows authenticated users
    #  to see which companies they're currently monitoring.
    if request.user.is_authenticated():
        user = request.user.id
        response = user.companies_monitored.values()
    else:
        response = {"error": "Unauthorized request."}
    return JsonResponse(response)

def company_stats_view(request):
    return render(request, 'companies/company_stats.html')
